import {Store} from 'core';

import {reducers} from './reducer';

class AppStore extends Store {
  /**
   * Create store
   * @param  {Object} initialState  {Initial state}
   * @param  {Object} history       {Browser history}
   * @return {Object}               {Front store}
   */
  create(initialState, history) {
    return super.create(reducers, initialState, history);
  }
}

export const store = new AppStore();