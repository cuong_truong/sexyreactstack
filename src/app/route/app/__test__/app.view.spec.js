import { Switch } from 'react-router-dom';

import {shallowIntl} from 'test-util';

import {appView} from '../app.view';

/**
* Test render()
* @return {Void}
*/
const testRender = () => {
  it('render()', () => {
    // arrange
    const {view} = appView;
    const route = { routes: [
      { path: '/', component: null },
      { path: '/home', component: null }
      ] };
    const expectedRoutesLength = 2;

    // sut
    const wrapper = shallowIntl(view({route}));
    const appComponent = wrapper.children('.app');
    const switchComponent = wrapper.children(Switch);
    const routeComponents = switchComponent.children();

    // expect
    assert.isNotNull(view);

    assert.isNotNull(wrapper);
    assert.isNotNull(appComponent);
    assert.isNotNull(switchComponent);
    assert.deepEqual(routeComponents.length, expectedRoutesLength);
  });
};

describe('app.view.spec.js', () => {
  testRender();
});