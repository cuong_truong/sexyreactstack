import Immutable from 'immutable';
import _ from 'lodash';

import { CLIENT_INIT } from 'redux/store';

import { ACTION_TOGGLE_SIDEBAR } from './app.constant';

const initialState = Immutable.fromJS({
  error: null,
  isLoading: false,
  isSidebarOpen: false
});

/**
 * Initialize the redux store when the client init
 *
 * @param {Object} state The redux state
 * @return {Object}      New state
 */
export const handleClientInit = (state) => {
  return state.setIn(['isSidebarOpen'], true);
};

/**
 * Handle the sidebar toggle action
 *
 * @param {Object} state The redux state
 * @return {Object}      New state
 */
export const handleToggleSidebar = (state, { isSidebarOpen }) => {
  const currentIsSidebarOpen = state.getIn(['isSidebarOpen']);
  const newIsSidebarOpen = _.isNil(isSidebarOpen) ? !currentIsSidebarOpen : isSidebarOpen;

  return state.setIn(['isSidebarOpen'], newIsSidebarOpen);
};

/**
 * App reducer
 *
 * @param  {Object} state  Current state
 * @param  {Object} action Redux action
 * @return {Object}        New state
 */
export const appReducer = (state = initialState, { type, payload = {} } = {}) => {
  switch (type) {
    case CLIENT_INIT:
      return handleClientInit(state);
    case ACTION_TOGGLE_SIDEBAR:
      return handleToggleSidebar(state, payload);
    default:
      return state;
  }
};
