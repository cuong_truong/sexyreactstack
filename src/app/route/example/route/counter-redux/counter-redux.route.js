import { asyncComponent } from 'core';

import { store } from 'app/redux/store';

import CounterRedux from 'bundle-loader?lazy!./counter-redux.component';
import { URL_COUNTER_REDUX, REDUCER_COUNTER_REDUX } from './counter-redux.constant';
import { counterReduxReducer } from './counter-redux.reducer';

/**
 * InjectReducer
 * @return {void}
 */
export const injectReducer = () => {
  store.injectReducer(REDUCER_COUNTER_REDUX, counterReduxReducer);
};

/**
 * Lazy load CounterRedux component
 */
const component = asyncComponent({
  component: CounterRedux,
  preProcess: injectReducer
});

/**
 * Counter redux route
 */
export const counterReduxRoute = {
  path: URL_COUNTER_REDUX,
  component
};
