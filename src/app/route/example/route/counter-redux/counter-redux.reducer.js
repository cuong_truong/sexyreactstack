import Immutable from 'immutable';

import { INT_ZERO, INT_ONE } from 'core';

import {
  ACTION_INCREASE,
  ACTION_DECREASE,
  REDUCER_COUNTER_REDUX_VALUE
} from './counter-redux.constant';

const initialState = Immutable.fromJS({
  [REDUCER_COUNTER_REDUX_VALUE]: INT_ZERO
});

/**
 * Handle increase counter
 * @param {object} state The counter state
 * @return {object}      The new counterstate
 */
export const handleIncrease = (state) => {
  return state.setIn([REDUCER_COUNTER_REDUX_VALUE], state.getIn([REDUCER_COUNTER_REDUX_VALUE]) + INT_ONE);
};

/**
 * Handle decrease counter
 * @param  {object} state The counter state
 * @return {object}      The new counterstate
 */
export const handleDecrease = (state) => {
  return state.setIn([REDUCER_COUNTER_REDUX_VALUE], state.getIn([REDUCER_COUNTER_REDUX_VALUE]) - INT_ONE);
};

/**
 * Counter reducer
 *
 * @param  {object} state  Current state
 * @param  {object} action Redux action
 * @return {object}        New state
 */
export const counterReduxReducer = (state = initialState, { type }) => {
  switch (type) {
    case ACTION_INCREASE:
      return handleIncrease(state);
    case ACTION_DECREASE:
      return handleDecrease(state);
    default:
      return state;
  }
};
