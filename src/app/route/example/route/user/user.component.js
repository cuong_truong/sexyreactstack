
import { list } from 'react-immutable-proptypes';
import { array } from 'prop-types';
import { connect } from 'react-redux';

import { BaseComponent, view } from 'core';

import { userView } from './user.view';
import { userSelector } from './user.selector';
import { loadUsers } from './user.action';

@connect(userSelector, {loadUsers})
@view(userView)
export default class User extends BaseComponent {
  /**
   * Define propTypes
   */
  static propTypes = {
    users: array
  }

  /**
   * Handle on click button Get Users
   * @return {void}
   */
  onClick = () => {
    this.props.loadUsers && this.props.loadUsers();
  }

  /**
   * Model for view
   * @return {object} Model object for view
   */
  get model() {
    return this.createModel();
  }

  /**
   * Handler for view
   * @return {object} Handler for view
   */
  get handler() {
    return this.createHandler();
  }
}