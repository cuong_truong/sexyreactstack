import { URL_EXAMPLE_USER } from '../../example.constant';

/* Urls */
export const URL_USER = URL_EXAMPLE_USER;

/* Actions */
export const ACTION_GET_USERS_REQUEST = 'ACTION_GET_USERS_REQUEST';
export const ACTION_GET_USERS_SUCCESS = 'ACTION_GET_USERS_SUCCESS';
export const ACTION_GET_USERS_FAILURE = 'ACTION_GET_USERS_FAILURE';

/* Reducer */
export const REDUCER_USER = 'user';
export const REDUCER_USER_LIST = 'list';