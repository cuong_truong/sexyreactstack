import { CALL_API } from 'core';

import {
  ACTION_GET_USERS_REQUEST,
  ACTION_GET_USERS_SUCCESS,
  ACTION_GET_USERS_FAILURE
} from './user.constant';

/**
 * Fetch users from fake api
 * @return {object}
 */
const getUsers = () => {
  return {
    [CALL_API]: {
      // endpoint: 'http://jsonplaceholder.typicode.com/users',
      endpoint: 'http://jsonplaceholder.typicode.com/albums/1/photos',
      method: 'GET',
      types: [ACTION_GET_USERS_REQUEST, ACTION_GET_USERS_SUCCESS, ACTION_GET_USERS_FAILURE]
    }
  };
};

/**
 * Load users
 *
 * @return {[type]}
 */
export const loadUsers = () => {
  return (dispatch) => {
    return dispatch(getUsers());
  };
};
