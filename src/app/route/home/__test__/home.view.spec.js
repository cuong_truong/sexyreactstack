import React from 'react';

import {shallowIntl} from 'test-util';
import {homeView} from '../home.view';

/**
* Test render()
* @return {Void}
*/
const testRender = () => {
  it('render()', () => {
    // arrange
    const {view} = homeView;

    const children = <img src='/asset/image/confident-girls.jpg'/>;
    const expectedGridHomeElementLength = 1;

    // sut
    const wrapper = shallowIntl(view({children}));
    const gridHomeComponent = wrapper.children('#Home');

    // expect
    assert.isNotNull(view);

    assert.isNotNull(wrapper);
    assert.deepEqual(gridHomeComponent.length, expectedGridHomeElementLength);
  });
};

describe('home.view.spec.js', () => {
  testRender();
});