/* Empty constants */
export const EMPTY_FUNCTION = () => {};

// Text
export const TEXT_DAYS = 'days';
export const TEXT_LEFT = 'left';
export const TEXT_CENTER = 'center';
export const TEXT_RIGHT = 'right';

/* Date format */
export const DATE_FORMAT = 'D MMM YYYY';
