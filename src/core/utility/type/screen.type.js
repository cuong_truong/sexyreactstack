// @flow

export type ScreenSizes = {
  width: number,
  height: number
};
