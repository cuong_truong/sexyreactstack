import thunk from 'redux-thunk';
import {createStore, applyMiddleware, compose} from 'redux';
import {routerMiddleware} from 'react-router-redux';
import {persistState} from 'redux-devtools';
import createHistory from 'history/createBrowserHistory';

import {createReducer} from './reducer';
import {apiMiddleware} from './middleware';
import {DevTools} from './dev-tools';

export const CLIENT_INIT = 'CLIENT_INIT';

export class Store {
  constructor() {
    if (!Store.instance) {
      Store.instance = this;
    }

    return Store.instance;
  }

  /**
  * Config store with middlewares/logger/devtools
  * @param  {Function}  rootReducer     Root reducer
  * @param  {Object}    initialState    Initial state
  * @param  {Object}    history         Browser history
  * @return {Object}                    Object Store
  */
  config(rootReducer, initialState, history = createHistory()) {
    // Sync dispatched route actions to the history
    const reduxRouterMiddleware = routerMiddleware(history);
    let middlewares = [thunk, apiMiddleware, reduxRouterMiddleware];
    let finalCreateStore = null;

    // Case DevTools is enabled in webpack/config files
    if (__DEVTOOLS__) {
      finalCreateStore = compose(applyMiddleware(...middlewares), window.devToolsExtension
        ? window.devToolsExtension()
        : DevTools.instrument(), persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/)))(createStore);
    } else {
      finalCreateStore = applyMiddleware(...middlewares)(createStore);
    }

    return finalCreateStore(rootReducer, initialState);
  }

  /**
   * Create store
   * @param  {Object} reducers      Reducers
   * @param  {Object} initialState  Initial state
   * @param  {Object} history       Browser history
   * @return {Object}               Application store
   */
  create(reducers, initialState, history) {
    const rootReducer = createReducer(reducers);

    this.store = this.config(rootReducer, initialState, history);
    this.store.reducers = reducers || {};

    if (__DEVELOPMENT__ && module.hot) {
      module
        .hot
        .accept('./reducer', () => {
          this
            .store
            .replaceReducer(rootReducer);
        });
    }

    return this.store;
  }

  /**
   * Dynamic inject async reducer to store
   * @param  {String}   name              Reducer name
   * @param  {Function} reducer           Reducer
   * @param  {Object}   configuredStore   Store. Defaults to current store
   * @return {Void}
   */
  injectReducer(name, reducer, configuredStore = this.store) {
    if (configuredStore && name && reducer) {
      configuredStore.reducers[name] = reducer;
      configuredStore.replaceReducer(createReducer(configuredStore.reducers));
    }
  }
}