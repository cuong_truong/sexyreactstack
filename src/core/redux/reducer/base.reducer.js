import Immutable from 'immutable';

export class BaseReducer {
  name = 'base';
  initialState = Immutable.fromJS({});

  canHandle = (type) => this.actionHandler && Boolean(this.actionHandler[type]);
  getNewState = (currentAppState, { type, payload }) => {
    if (!this.canHandle(type)) {
      return currentAppState;
    }

    const currentState = currentAppState.get(this.name) || this.initialState;
    const newState = this.actionHandler[type](currentState, payload);

    return currentAppState.set(this.name, newState);
  };
}
