import React from 'react';
import { shallow } from 'enzyme';

import { IntlProvider, intlShape } from 'core';
import { BaseContextBuilder } from './base.context';

const TEXT_RESOURCES = __RESOURCES__;

class IntlContextBuilder extends BaseContextBuilder {

  hasContext(node) {
    if (!node) {
      return false;
    }
    const {contextTypes} = node.type;

    return contextTypes && contextTypes.intl !== null;
  }

  createContext(options) {
    const {messages = TEXT_RESOURCES, locale = 'en'} = options;
    const props = {
      locale,
      messages
    };
    const intlProviderWrapper = shallow(<IntlProvider.WrappedComponent {...props} />);
    const instance = intlProviderWrapper.instance();

    const {intl} = instance.getChildContext();

    return {
      context: {
        intl
      },
      childContextTypes: {
        intl: intlShape.isRequired
      }
    };
  }
}

export const intlBuilder = new IntlContextBuilder();
