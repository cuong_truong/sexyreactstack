import { shallow as shallowEnzyme, mount as mountEnzyme } from 'enzyme';

import { buildContext } from './context';

const shallow = (node, options) => {
  const context = buildContext(node, options);

  return shallowEnzyme(node, context);
};

const mount = (node, options) => {
  const newOptions = Object.assign({}, options, {
    forceBuild: true
  });
  const context = buildContext(node, newOptions);

  return mountEnzyme(node, context);
};

export { shallow, mount };