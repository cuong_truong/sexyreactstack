const helper = require('../helper');

/* Paths info */
const paths = {
  // Function to join paths
  join: helper.join,
  // <root>/
  root: helper.root(),
  // <root>/_dist
  dist: helper.root('_dist'),
  // <root>/_dist/app
  distApp: helper.root('_dist', 'app'),
  // <root>/_dist/app
  distAppFont: helper.root('_dist', 'app', 'asset', 'font'),
  // <root>/_dist/core
  distCore: helper.root('_dist', 'core'),
  // <root>/_dist
  report: helper.root('_report'),
  // <root>/config
  config: helper.root('config'),
  // <root>/server
  sever: helper.root('server'),
  // <root>/server
  appViewTemplate: helper.root('server', 'app', 'views', 'index.pug'),
  // <root>/flow-typed
  flowTyped: helper.root('flow-typed'),
  // <root>/src
  source: helper.root('src'),
  // <root>/src/app
  app: helper.root('src', 'app'),
  // <root>/src/core
  core: helper.root('src', 'core'),
  // <root>/src/app/asset
  coreAsset: helper.root('src', 'core', 'asset')
};

/* Urls info */
const urls = {
  image: helper.url('asset', 'image'),
  font: helper.url('asset', 'font'),
  style: helper.url('asset', 'style')
};

/* Hosts info */
const hosts = {
  app: process.env.HOST || '0.0.0.0'
};

/* Ports info */
const ports = {
  app: process.env.PORT || 8000,
  prod: process.env.PORT_PROD || 8001,
  test: process.env.PORT_TEST || 9000
};

/* Export info */
module.exports = {
  paths,
  urls,
  hosts,
  ports
};
