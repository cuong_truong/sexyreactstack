const firstCharIndex = 0;
const sliceBeginIndex = 1;

const childProcess = require('child_process');
const currentDirectoryPath = process.cwd();
const directoryPathRoot = currentDirectoryPath.charAt(firstCharIndex).toLowerCase();
const directoryPathTail = currentDirectoryPath.slice(sliceBeginIndex);
const convertedDirectoryPath = `${directoryPathRoot}${directoryPathTail}`;

try {
  // eslint-disable-next-line no-magic-numbers, no-sync
  childProcess.execSync(`flow status ${convertedDirectoryPath}`, { stdio: [0, 1, 2] });
}
catch (e) {
  // eslint-disable-next-line no-console
  console.log('Flow status ended!');
}
