const path = require('path');
const webpack = require('webpack');

const baseConfig = require('./base');
const resources = require('server/app/resources/i18n/en.json');

const { paths, sassLoaders } = baseConfig.settings;

/**
 * Get base Test config
 * @param  {String} sourceFolderName [Source Folder: app/app]json
 * @return {Object}                  [Object configuration]
 */
module.exports = sourceFolderName => {
  const environment = process.env.NODE_ENV;
  const postLoaders = [];

  /* Run test coverage only for CI, not in watch mode */
  if (environment === 'test') {
    postLoaders.push({
      test: /\.js$/,
      loader: 'istanbul-instrumenter-loader',
      include: new RegExp(`src.(${sourceFolderName})`),
      exclude: [
        // Exclude unit test files
        /(spec|index)\.js$/,
        // Exclude test, index and core/redux files
        new RegExp(`src.*.redux`),
        // Exclude styleguide page
        new RegExp(`src.app.route.styleguide`),
        // Exclude app.component due to issue wrong coverage
        new RegExp(`src.*.route.app.app.component.js`)
      ],
      enforce: 'post',
      options: {
        esModules: true,
        produceSourceMap: true
      }
    });
  }

  return {
    // just do inline source maps instead of the default
    devtool: 'inline-source-map',
    resolve: {
      modules: [
        paths.source,
        paths.core,
        path.join(paths.config, 'test'),
        'node_modules'
      ],
      extensions: ['.js', '.scss']
    },
    module: {
      rules: [
        ...postLoaders, {
          test: /\.js$/,
          loader: 'babel-loader?cacheDirectory',
          exclude: /node_modules/
        },

        /* json */
        {
          test: /\.css$/,
          loader: 'style-loader!css-loader'
        },

        /* scss */
        {
          test: /\.scss$/,
          loaders: ['style-loader', ...sassLoaders],
          include: new RegExp(`core|${sourceFolderName}`)
        },

        // images
        {
          test: /\.(jpg|jpeg|gif|png|ico)$/,
          loader: 'url-loader?limit=12288&name=asset/image/[name].[ext]'
        },

        // fonts
        {
          test: /\.(woff|woff2|eot|ttf|svg).*$/,
          loader: 'url-loader?name=asset/font/[name].[ext]'
        }
      ]
    },
    externals: {
      'react/addons': true,
      'react/lib/ExecutionEnvironment': true,
      'react/lib/ReactContext': true
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('test'),
        __DEVELOPMENT__: false,
        /* DISABLE redux-devtools HERE */
        __DEVTOOLS__: false,
        __RESOURCES__: JSON.stringify(resources)
      })
    ]
  };
};
