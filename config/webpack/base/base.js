const webpack = require('webpack');
const variables = require('../../variable');
const { paths, urls, hosts, ports } = variables;
const webpackHotMiddleWare = 'webpack-hot-middleware/client?reload=true';

module.exports = {
  settings: {
    paths,
    urls,
    hosts,
    ports,
    sassLoaders: [
      {
        loader: 'css-loader'
      },
      {
        loader: 'autoprefixer-loader?{browsers:["last 2 versions", "ie 6-8", "Firefox > 20"]}'
      },
      {
        loader: 'sass-loader',
        options: {
          includePaths: [paths.coreAsset]
        }
      }
    ],
    webpackHotMiddleWare,
    host: hosts.default,
    port: ports.default
  },
  webpack: {
    resolve: {
      modules: [
        paths.source,
        paths.core,
        'node_modules'
      ],
      extensions: ['.js', '.html', '.scss']
    },
    plugins: [
      /* Workaround for jsondiffpatch */
      new webpack.ContextReplacementPlugin(
        // The (\\|\/) piece accounts for path separators in *nix and Windows
        /jsondiffpatch/
      ),
      new webpack.NoEmitOnErrorsPlugin(),
      /* Used to split out our sepcified vendor script */
      new webpack.optimize.CommonsChunkPlugin({
        names: ['webpack', 'vendor'],
        filename: 'js/[name].js',
        publicPath: '/',
        minChunks: ({ resource }) => {
          const INDEX_NOT_FOUND = -1;

          // this assumes your vendor imports exist in the node_modules directory
          return resource &&
            resource.indexOf('node_modules') !== INDEX_NOT_FOUND &&
            resource.match(/\.js$/);
        }
      }),
      /* CommonChunksPlugin will now extract all the common modules from vendor, app, webpack,... bundles */
      new webpack.optimize.CommonsChunkPlugin({
        /* But since there are no more common modules between them we end up with
         * just the runtime code included in the manifest file
         */
        name: 'manifest',
        minChunks: Infinity
      })
    ]
  }
};