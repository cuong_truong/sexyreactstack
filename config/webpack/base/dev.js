const webpack = require('webpack');
const WebpackBrowserPlugin = require('webpack-browser-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const baseConfig = require('./base');

module.exports = {
  /**
   * Get style loaders for appropriate project in DEVELOPMENT mode
   * @param  {String} sourceFolderName [Source folder: app/front]
   * @return {Array}                   [Array of Style loaders that project uses]
   */
  styleLoaders(sourceFolderName) {
    return [
      /* CSS */
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
        exclude: /src/
      },

      /* SCSS */
      {
        test: /\.scss$/,
        loaders: [
          'style-loader',
          ...baseConfig.settings.sassLoaders
        ],
        include: new RegExp(`core|${sourceFolderName}`)
      }
    ];
  },
  /**
   * Get plugins for appropriate project in DEVELOPMENT mode
   * @param  {Int} port    [Port number]
   * @return {Array}       [Array of plugins that project uses]
   */
  plugins: (port) => [
    new webpack.HotModuleReplacementPlugin(),

    /* Auto open browser */
    new WebpackBrowserPlugin({
      port,
      browser: 'Chrome',
      url: 'http://localhost'
    }),

    /* Circular dependency checking */
    new CircularDependencyPlugin({
      // exclude detection of files based on a RegExp
      exclude: /a\.js|node_modules/,
      // add errors to webpack instead of warnings
      failOnError: true
    }),

    /* Setting DefinePlugin affects React library size! */
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
      __DEVELOPMENT__: true,
      /* DISABLE redux-devtools HERE */
      __DEVTOOLS__: true,
      /* DISABLE "Download the React DevTools for a better development experience: https://fb.me/react-devtools" */
      __REACT_DEVTOOLS_GLOBAL_HOOK__: {}
    })
  ]
};
