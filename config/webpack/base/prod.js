const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');

const baseConfig = require('./base');
const { paths, urls, sassLoaders } = baseConfig.settings;

module.exports = {
  /**
   * Extract plugins
   * @return {Object} [Extract Text plugin including: app.css & lib.css]
   */
  extractPlugins() {
    const lib = new ExtractTextPlugin(`${urls.style}/libs.css`);
    const app = new ExtractTextPlugin(`${urls.style}/app.css`);

    return { lib, app };
  },

  /**
   * Get style loaders for appropriate project in PRODUCTION mode
   * @param  {Object} extractTextplugin   [Extract Text plugin]
   * @param  {String} sourceFolderName    [Source folder: app/front]
   * @return {Array}                      [Array of Style loaders that project uses]
   */
  styleLoaders({ lib, app }, sourceFolderName) {
    return [{
      test: /\.css$/,
      loader: lib.extract({
        fallback: 'style-loader',
        use: 'css-loader'
      }),
      exclude: paths.source
    }, {
      test: /\.scss$/,
      loader: app.extract({
        fallback: 'style-loader',
        use: sassLoaders
      }),
      include: new RegExp(`core|${sourceFolderName}`)
    }];
  },
  /**
   * Get plugins for appropriate project in PRODUCTION mode
   * @param  {string} distFontPath    [The built Font path]
   * @return {Array}                  [Array of plugins that project uses]
   */
  plugins: distFontPath => ([
    /* Uglify JS */
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: {
        warnings: false,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        evaluate: true,
        dead_code: true,
        screw_ie8: true,
        if_return: true,
        join_vars: true
      },
      output: {
        comments: false
      }
    }),

    /* Copy core fonts */
    new CopyWebpackPlugin([{
      from: paths.join(paths.coreAsset, 'font'),
      to: distFontPath
    }]),

    /* Setting DefinePlugin affects React library size! */
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
      __DEVELOPMENT__: false,
      /* DISABLE redux-devtools HERE */
      __DEVTOOLS__: false
    })
  ]),
  stats: {
    assets: true,
    cached: true,
    children: false,

    // Add chunk information (setting this to `false` allows for a less verbose output)
    chunks: true,
    // Add built modules information to chunk information
    chunkModules: true,
    colors: true,
    // Add errors
    errors: true,
    // Add details to errors (like resolving log)
    errorDetails: true,
    timings: true,
    warnings: true
  }
};
