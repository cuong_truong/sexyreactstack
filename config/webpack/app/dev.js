const merge = require('webpack-merge');

const devConfig = require('config/webpack/base/dev');
const baseConfig = require('./base');

const { webpackHotMiddleWare, sourceFolderName, ports } = baseConfig.settings;

module.exports = merge(baseConfig, {
  webpack: {
    // https://github.com/webpack/docs/wiki/configuration#devtool
    devtool: 'source-map',
    entry: {
      webpack: [webpackHotMiddleWare]
    },
    module: {
      rules: [
        ...devConfig.styleLoaders(sourceFolderName)
      ]
    },
    plugins: devConfig.plugins(ports.app)
  }
});
