const merge = require('webpack-merge');
const CleanPlugin = require('clean-webpack-plugin');

const prodConfig = require('../base/prod');
const baseConfig = require('./base');

const { sourceFolderName, paths } = baseConfig.settings;
const extractPlugins = prodConfig.extractPlugins();

module.exports = merge(baseConfig, {
  webpack: {
    // Turn on devtool if you want to debug on production
    // devtool: 'cheap-module-eval-source-map',
    stats: prodConfig.stats,

    module: {
      rules: [...prodConfig.styleLoaders(extractPlugins, sourceFolderName)]
    },

    plugins: [
      // Clean build folder
      new CleanPlugin([paths.distApp], {
        root: paths.root
      }),

      // Output extracted CSS to files
      extractPlugins.lib,
      extractPlugins.app,

      // plugins
      ...prodConfig.plugins(paths.distAppFont)
    ]
  }
});
