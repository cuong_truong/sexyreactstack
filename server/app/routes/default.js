const express = require('express');
const pug = require('pug');
const path = require('path');
const router = express.Router();

const checkAuthentication = require('server/core/auth');
const messages = require('../resources/i18n/en.json');

const initialState = {
  app: {
    localization: {
      messages,
      defaultLocale: 'en',
      locale: 'en'
    },
    isLoading: false,
    error: null,
    sidebar: {}
  }
};

router.get('', (req, res) => {
  if (!checkAuthentication(req, res)) {
    return;
  }

  const compiler = req.app ? req.app.get('compiler') : null;

  // Webpack compiler is available in DEV mode only
  // In DEV we don't have static/build folder, bundle files are in memory instead
  if (compiler) {
    const filename = path.join(compiler.outputPath, 'index.pug');

    compiler.outputFileSystem.readFile(filename, 'utf8', (err, pugString) => {
      if (err) {
        res.json(err);

        return;
      }

      const html = pug.render(pugString, { initialState });

      res.set('content-type', 'text/html');
      res.send(html);
      res.end();
    });
  }
  else {
    res.render('index', { initialState });
  }
});

module.exports = ['*', router];
